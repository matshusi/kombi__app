import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = 'https://c315732e.ngrok.io/Suggestion';


@Injectable({
  providedIn: 'root'
})
export class ContributeserviceService {

  constructor(private http: HttpClient) { }
  postContribution(a: string, address: string, latitude: number, longitude: number, price: number,) {
    let data = {
      suggeOrigin: address,
      lat: latitude,
      lng: longitude,
      description: a,
      taxiFare: price,
    }
    console.log(data);
    return this.http.post(API_URL + '/add', data);

  }
}
