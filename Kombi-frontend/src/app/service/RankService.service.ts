import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


const API_URL = 'https://c315732e.ngrok.io/ranks';

@Injectable({
  providedIn: 'root'
})

export class RankServiceService {

  selectedValue: string;
  id: number;
  selectedLat  : any;
  selectedLong  : any;
  selectedrankName  : any;
  selectedRoute  : any;
  selectedFareprice : any;
  selectedDestName  : any;
  selectedDesLat  : any;
  selectedDesLng  : any;
  selectedDesStreetName  : any;
  selectedId: any;
  destLng: any;
  destLat: any;
  selectedLane: any;
  selectedStreetName: string;

  

  constructor(private http: HttpClient  ) { }


  findAllRanks(): Observable<any> {
    return this.http.get(API_URL + '/all',
  {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }




  getAllRanks(id): Observable<any> {
    return this.http
      .get<any>(API_URL +'/'+ id)
    
  }

}
