import { TestBed } from '@angular/core/testing';

import { ContributeserviceService } from './contributeservice.service';

describe('ContributeserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContributeserviceService = TestBed.get(ContributeserviceService);
    expect(service).toBeTruthy();
  });
});
