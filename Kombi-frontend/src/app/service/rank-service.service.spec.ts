import { TestBed } from '@angular/core/testing';

import { RankServiceService } from './rank-service.service';

describe('RankServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RankServiceService = TestBed.get(RankServiceService);
    expect(service).toBeTruthy();
  });
});
