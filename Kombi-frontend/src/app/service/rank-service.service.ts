import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'https://c315732e.ngrok.io/ranks';


@Injectable({
  providedIn: 'root'
})
export class RankServiceService {
  selectedValue: string;
  id: number;
  selectedStreetName  : string;
  selectedLat  : string;
  selectedLong  : string;
  selectedrankName  : string;
  selectedRoute  : string;
  selectedFareprice : string;
  selectedDestName  : string;
  selectedDesLat  : string;
  selectedDesLong  : string;
  selectedDesStreetName  : string;
  selectedId: string;
  constructor(private http: HttpClient) { }


  findAllRanks(): Observable<any> {
    return this.http.get(API_URL + '/all',
  {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }


  // getAllRanks(id): Observable<any> {
  //   return this.http.get( API_URL +'${1}',
  // {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  // }

  getAllRanks(id): Observable<any> {
    return this.http
      .get<any>(API_URL +'/'+ id)
      // .pipe(
      //   retry(2),
      //   catchError(this.handleError)
      
  }
}
