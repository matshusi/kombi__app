import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReviewserviceService {

  constructor(private http: HttpClient) { }

  postReview(a: string) 
  {
    console.log(a);
    //localhost:8080//Reviews/add
    //return this.http.post(SERVER_URL + '/Reviews/add', { review: a });

    return this.http.post('https://c315732e.ngrok.io/add',
     {
       reviews: a,
       likes:100,
       dislikes:90
   });
  }

  getReview() {
    //return this.http.get('https://jsonplaceholder.typicode.com/posts');
    return this.http.get('https://c315732e.ngrok.io/all');
  }


}

