import { Component, OnInit } from '@angular/core';
import { ReviewserviceService } from '../service/reviewservice.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.page.html',
  styleUrls: ['./reviews.page.scss'],
})
export class ReviewsPage implements OnInit {

  reviews : Array<any>=[]
  userReviews;
  review = '';

  constructor(private reviewservice: ReviewserviceService ,public menuCtrl: MenuController ) { }

  async ngOnInit() {
    this.getReview();
 }

 public postDB(index) {
    console.log(index);
    this.reviews.push(this.review);
   // this.dataService.getReview().subscribe(data => console.log(data));
    this.reviewservice.postReview(this.review ).subscribe(data => console.log(data));
 }

 getReview()
 {
    this.reviewservice.getReview().subscribe(data =>{
      this.userReviews=data;

      for(let i=0; i<this.userReviews.length; i++)
      {
       console.log(this.userReviews[i].reviews);
       this.reviews.push(this.userReviews[i].reviews);
      }
      
    });
}

// async presentPopover(ev: any) {
//  const popover = await this.menuCtrl.({
//    component: MenuController,
//    event: ev,
//    translucent: true
//  });
//  return await popover.present();
// }


}
