import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ContributeComponent } from '../rank-list/contribute/contribute.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  constructor(
    public popoverCtrl: PopoverController
  ) {}

  ngOnInit() {}

  closeMenu () {
    this.popoverCtrl.dismiss();
  }
  async contributor() {
    const modal = await this.popoverCtrl.create({
      component: ContributeComponent,
    });
    return await modal.present();
  }

}
