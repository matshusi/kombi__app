import { ContributeComponent } from '../rank-list/contribute/contribute.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { async } from 'q';
import { RankDetailsComponent } from '../rank-list/rank-details/rank-details.component';
//import { Storage } from '@ionic/storage';
import { MenuComponent } from '../menu/menu.component';
import { PopoverController } from '@ionic/angular';
import { RankServiceService } from 'src/app/service/rank-service.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';



@Component({
  selector: 'app-rank-list',
  templateUrl: './rank-list.page.html',
  styleUrls: ['./rank-list.page.scss'],
})
export class RankListPage implements OnInit{
  data:any;
  Ranks: any;

  Contributors = [{ rankLocation: 'Leondale', streetName: '99 Plein Street', Contributor: 'Ngwane Muni' },
  { rankLocation: 'Gomora', streetName: '99 Plein Street', Contributor: 'Theo Bav' },
  {  rankLocation: 'Lwandle', streetName:'99 Plein Street', Contributor: 'Toriso Molepo' },
  { rankLocation:  'Soekmekaar', streetName: '99 Plein Street', Contributor: 'Kingsley Jiks' },
  { rankLocation:  'Elandsfontein', streetName: '99 Plein Street', Contributor: 'George Ntsoana' },

  ]
  constructor(
     public Service: RankServiceService,
     public modalController: ModalController,
     private localStorageService:LocalstorageService,
     public popoverCtrl: PopoverController,) { 

      this.initiliazeData();
    }

  public initiliazeData()
  {
    this.Service.findAllRanks().subscribe(ranks => {
       this.Ranks = ranks;
      ranks.sort(function(a, b){
        if(a.rankLocation < b.rankLocation) { return -1; }
        return 0;
        
    })
    console.log(ranks);
   });
   
  }
  list=[];
  //seaching Ranks
  filterData(ev:any){

   
    const val= ev.target.value;

    if(val && val.trim() != ''){

      this.list =this.Ranks.filter((item)=>{
        return (item.rankLocation.toLowerCase().indexOf(val.toLowerCase()) > -1);

      }) || (
        this.Contributors = this.Contributors.filter((item)=>{
        return (item.rankLocation.toLowerCase().indexOf(val.toLowerCase()) > -1);

      }))
      this.Ranks=this.list;
    }
    

  }
  ngOnInit() {
    //this.storage.set('name', 'malome data');

    // this.localStorageService.setLocal(this.Ranks)
    // this.localStorageService.setLocal(this.Contributors);
    // this.localStorageService.refreshStorageData();

    // console.log(this.localStorageService.storageData)

  }


  async menuPopover(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: MenuComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  async viewDetails(items, index: number) {
    const modal = await this.modalController.create({
      component: RankDetailsComponent,
      componentProps: { items, index }
    });
    return await modal.present();
  }



  async contributor() {
    const modal = await this.modalController.create({
      component: ContributeComponent,
    });
    return await modal.present();
  }


}
