import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RankListPage } from './rank-list.page';

describe('RankListPage', () => {
  let component: RankListPage;
  let fixture: ComponentFixture<RankListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RankListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
