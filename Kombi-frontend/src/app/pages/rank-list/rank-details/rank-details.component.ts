import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-rank-details',
  templateUrl: './rank-details.component.html',
  styleUrls: ['./rank-details.component.scss'],
})
export class RankDetailsComponent implements OnInit {
  numberOfLikes : number = 0;
  selectedIndex: number;
  selectedItem;
  constructor(private navParms: NavParams, private modalCtrl: ModalController) { 
    this.selectedItem = this.navParms.data.items;
    this.selectedIndex = this.navParms.data.index;
  }
  ngOnInit() {
  console.log(this.selectedItem);
  }
  likeButtonClick(){
    this.numberOfLikes++;
  }

  dislikeButtonClick(){
    if(this.numberOfLikes > 0){
      this.numberOfLikes--;
    };
    
  }

  modalDismiss(){
    this.modalCtrl.dismiss();
  }
}


