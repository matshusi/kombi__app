import { ModalController } from '@ionic/angular';
import { Component, OnInit, ElementRef, ViewChild, NgZone  } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { ContributeserviceService } from 'src/app/service/contributeservice.service';

@Component({
  selector: 'app-contribute',
  templateUrl: './contribute.component.html',
  styleUrls: ['./contribute.component.scss'],
})
export class ContributeComponent implements OnInit {
  destination: string = '';
  price: number;
  contribute: string = '';
  location: string = '';
  title: string = 'Contribute';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  

  @ViewChild('search', {static: true})
  public searchElementRef: ElementRef;
  map:any;
  authService: any;
  router: any;
  errorMessage: string;
  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public modalController: ModalController, private contriService:ContributeserviceService) { }

  ngOnInit() {

 this.mapsAPILoader.load().then(() => {
  this.setCurrentLocation();
  this.geoCoder = new google.maps.Geocoder;
});

  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
    }
    markerDragEnd($event: MouseEvent) {
      console.log($event);
      this.latitude = $event.coords.lat;
      this.longitude = $event.coords.lng;
      this.getAddress(this.latitude, this.longitude);
    }
  
    getAddress(latitude, longitude) {
      this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
        console.log(results);
        console.log(status);
        if (status === 'OK') {
          if (results[0]) {
            this.zoom = 12;
            this.address = results[0].formatted_address;
          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      
      });
    }
  
  postsuggestDB(contribute){
    console.log('address ', this.price, this.address, this.latitude, this.longitude);
    
    this.contriService.postContribution(contribute, this.address, this.latitude, this.longitude, this.price,  ).subscribe(data => console.log(data));
        
      }
      closeModal () {
        this.modalController.dismiss();
      }
    

}
