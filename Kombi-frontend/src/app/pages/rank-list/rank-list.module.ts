import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RankListPageRoutingModule } from './rank-list-routing.module';
import { RankListPage } from './rank-list.page';
import { RankDetailsComponent } from '../rank-list/rank-details/rank-details.component';
import { ContributeComponent } from '../rank-list/contribute/contribute.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAlz6igQGvuG9I1ug2A_9wze6Ywsvb42Co',
      libraries: ['places']
    }),
    RankListPageRoutingModule],
  entryComponents:[RankDetailsComponent, ContributeComponent],
  declarations: [RankListPage,RankDetailsComponent, ContributeComponent]
})
export class RankListPageModule {}
