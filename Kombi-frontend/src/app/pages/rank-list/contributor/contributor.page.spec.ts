import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContributePage } from './contributor.page';

describe('ContributorPage', () => {
  let component: ContributorPage;
  let fixture: ComponentFixture<ContributorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContributorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
