import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { RankServiceService } from 'src/app/service/RankService.service';



@Component({
  selector: 'app-whereto',
  templateUrl: './whereto.component.html',
  styleUrls: ['./whereto.component.scss'],

})
export class WheretoComponent implements OnInit {

  Ranks: any;
  selectedValue;

  current: any = {
    address: '',
    longitude: '',
    latitude: ''
  }
  searchContent: boolean;
 
 

  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public service : RankServiceService
  ) { 
   this.current = this.navParams.data;

   this.initiliazeData();

  }

  ngOnInit() {


    
   
  }

  closeModal () {
    this.modalCtrl.dismiss();
  }


  public initiliazeData()
              {
                
                this.service.findAllRanks().subscribe(ranks => {
                   this.Ranks = ranks;
                   ranks.sort(function(a, b){
                    if(a.rankLocation < b.rankLocation) { return -1; }
                  
                    return 0;
                })
                  console.log(ranks); // if it work plgitese remove
               });
               
              }


     //seaching Ranks
     filterData(ev:any){

      
               
      const val= ev.target.value;

      if(val && val.trim() != ''){

        this.Ranks =this.Ranks.filter((item)=>{
          return (item.rankLocation.toLowerCase().indexOf(val.toLowerCase()) > -1);

        }) 
      }
      

    }

 


  selectVal(val, id, latitude, longitude,rankname, price, lane , streetName, route , destRankName , destStreetName , destLat, destLng)
  {
    console.log(' id: ', id);
    console.log(' lat: ', latitude);
    console.log(' long: ', longitude);
    console.log(' streetname: ', streetName);
    console.log(' rankname : ', rankname);
    console.log(' price: ', price);
    console.log(' usual Route: ', route);
    console.log(' rankname : ', rankname);
    console.log(' dest name: ', destRankName);
    console.log(' dest street: ', destStreetName);
    console.log(' dest lat: ',destLat);
    console.log(' dest lng: ', destLng);

    // alert("You have selected = "+val);
    this.service.selectedValue = val;
    this.service.id = id;
    this.service.selectedLat =latitude;
    this.service.selectedLong =longitude;
    this.service.selectedrankName =rankname;
    this.service.selectedFareprice=price;
    this.service.selectedLane=lane;
    this.service.selectedStreetName=streetName;
    this.service.selectedRoute=route;
    this.service.selectedDestName =destRankName;
    this.service.selectedDesStreetName= destStreetName;
    this.service.selectedDesLat=destLat;
    this.service.selectedDesLng=destLng;
    
    this.selectedValue = val;


    // console.log(this.service.selectedValue);

    // console.log(this.service.selectedDesLat)
    

    

    this.closeModal();

  }






}