import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

import { HomePage } from './home.page';
import { WheretoComponent } from '../whereto/whereto.component';
import { NgPipesModule } from 'ngx-pipes';
import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule,
    AgmDirectionModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAlz6igQGvuG9I1ug2A_9wze6Ywsvb42Co',
      libraries: ['places']
    }),
    NgPipesModule
  ],
  declarations: [HomePage, WheretoComponent],
  entryComponents: [
    WheretoComponent
  ]
})
export class HomePageModule {}