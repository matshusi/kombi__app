import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone
} from "@angular/core";
import { PopoverController, ModalController } from "@ionic/angular";
import { MenuComponent } from "../menu/menu.component";
import { Platform } from "@ionic/angular";
import { WheretoComponent } from "../whereto/whereto.component";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";
import { MapsAPILoader, MouseEvent } from "@agm/core";
import { RankServiceService } from 'src/app/service/RankService.service';



@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit {
  title: string = "My first AGM project";
  lat: any;
  lng: any;
  longitude: any;
  latitude: any;
  destLat: any;
  destLng: any;
  height = 0;

  locationCoords: any;
  timetest: any;
  zoom: number;
  address: string;
  geoCoder;

  name: any;
  usualroute: any;
  price: any;
  going: any;
  lane: any;
  Ranks: string;

  selectedValue: string;
  id: number;
  streetName: string;
  destName: string;
  destStreetName: any;
  destLong: any;
  destLati: any;

  constructor(
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public androidPermissions: AndroidPermissions,
    public geolocation: Geolocation,
    public locationAccuracy: LocationAccuracy,
    private mapsAPILoader: MapsAPILoader,
    public service: RankServiceService
  ) {
    this.height = platform.height() - 100;

    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    };
    this.timetest = Date.now();
  }

  public interval = setInterval(() => {
    this.selectedValue = this.service.selectedValue;
    this.id = this.service.id;
    this.latitude = this.service.selectedLat;
    this.longitude = this.service.selectedLong;
    this.name = this.service.selectedrankName;
    this.price = this.service.selectedFareprice;
    this.lane = this.service.selectedLane;
    this.streetName = this.service.selectedStreetName;
    this.usualroute = this.service.selectedRoute;
    this.going = this.service.selectedValue;
    this.destName = this.service.selectedDestName;
    this.destStreetName = this.service.selectedDesStreetName;
    this.destLat = this.service.selectedDesLat;
    this.destLng = this.service.selectedDesLng;

    


    if (this.selectedValue) {
      clearInterval(this.interval);
    } 

    this.getDirection();
  }, 1000);
  

  dir = {};

  public getDirection() {

    let tempObj = {
      lat: parseFloat(this.latitude),
      lng: parseFloat(this.longitude)
    };

    let tembObjDest = {
      destLat: parseFloat(this.destLat),
      destLng: parseFloat(this.destLng)
    };

    console.table({
      lat: parseFloat(this.latitude),
      lng: parseFloat(this.longitude)
    });

    console.table({
      destLat: parseFloat(this.destLat),
      destLng: parseFloat(this.destLng)
    });
  

    // Get the information from the API
    this.dir = {
      origin: { lat: this.lat, lng: this.lng },
      destination: tempObj,
      waypoint:tembObjDest,
  
  }
 


  console.log('name',this.destLng)
}


  ngOnInit() {
  
    console.log('name',this.destLng)

    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder();
      this.setCurrentLocation();
    });
  }

  

  ngOnDestroy() {
    console.log("destroyed");
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 16;

        this.getAddress(this.lat, this.lng);
      });
    }
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results, status) => {
        // console.log(results);
        // console.log(status);
        if (status === "OK") {
          if (results[0]) {
            this.zoom = 12;
            this.address = results[0].formatted_address;
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      }
    );
  }


  

  // call WHERE TO... modal
  async whereToModal() {
    // this.selectedValue = '';
    // this.latitude = '';
    // this.longitude = '';
    // this.destLat = '';
    // this.destLng = '';
    // this.getDirection();
    // this.interval;

    const modal = await this.modalCtrl.create({
      component: WheretoComponent,
      cssClass: "whereToModal",
      componentProps: {
        address: this.address,
        latitude: this.lat,
        lontitude: this.lng
      }
    });
    return await modal.present();
  }

  // call popover menu
  async menuPopover(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: MenuComponent,
      event: ev,
      translucent: true,
      componentProps: {
        paramID: 123,
        paramTitle: "Test Title"
      }
    });

    return await popover.present();
  }
 
  
}